
CREATE TABLE "books" (
    "guid" UUID PRIMARY KEY,
    "title" VARCHAR(64) NOT NULL,
    "author" VARCHAR(64) NOT NULL,
    "isbn" TEXT,
    "genre" TEXT,
    "publication_year" DATE NOT NULL
);

CREATE TABLE "library_members" (
    "guid" UUID PRIMARY KEY,
    "name" VARCHAR(64) NOT NULL,
    "contact" VARCHAR(64) NOT NULL,
    "address" TEXT
);

CREATE TABLE "library_staff" (
    "guid" UUID PRIMARY KEY,
    "name" VARCHAR(64) NOT NULL,
    "contact" VARCHAR(64) NOT NULL,
    "position" TEXT
);

CREATE TABLE "borrowings" (
    "guid" UUID PRIMARY KEY,
    "book_id" UUID NOT NULL REFERENCES "books"("guid"),
    "member_id" UUID NOT NULL REFERENCES "library_members"("guid"),
    "staff_id" UUID NOT NULL REFERENCES "library_staff"("guid"),
    "borrowed" DATE NOT NULL,
    "return_date" DATE
);
